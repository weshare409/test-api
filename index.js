import express from "express"
const app = express()
const PORT = 5000|| process.env.PORT

const dummyData = [
    {
        id:1,
        "name":"mark"
    },
    {
        id:2,
        "name":"john"
    },
    {
        id:3,
        "name":"sam"
    },
    {
        id:4,
        "name":"mani"
    },
]

app.get('/',(req, res)=>{
    return res.json({"message":"your api is working fine, append /api/endpoint to access api data"})
})
app.get('/api/users',(req, res)=>{
    return res.json({data:dummyData});
})
app.listen(PORT,()=>{
    console.log(`listening on port ${PORT}`)
})